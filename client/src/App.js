import React, { Component } from 'react';
import './App.css';

import Header from './components/header/header'
import Sentences from './components/sentences/sentences'
import SimilarTable from './components/similarTable/similarTable'
import PasteComponent from "./components/pastecomponent/pastecomponent";

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            sentences: [],
            similarity_info: {
                target: '',
                similar: []
            }
        }
    }

    dataPasted(e){
        let plain_text = e.clipboardData.getData("text/plain");
        if (plain_text.length > 65536){
            alert('Data is too big sorry');
        } else {
            this.setState({sentences: plain_text.split(".").filter((snts) => snts.length)});
        }
    }

    processSimilarInfo(info){
        this.setState({similarity_info: info});
    }

    clearSimilarityInfo(){
        this.setState({similarity_info: {target: '', similar: []}});
    }

    clearSentences(){
        this.setState({sentences: []});
    }

    setNewSentences(sentences){
        this.setState({sentences})
    }

    componentDidMount() {
        window.addEventListener('paste', (() => this.dataPasted.bind(this))());
    }

    render() {
        return <div className="App">
            <Header/>
            <div className="jumbotron">
                {this.state.sentences.length > 0 || <PasteComponent sentenceHandler={this.setNewSentences.bind(this)}/>}
                {this.state.similarity_info.target.length > 0 ?
                    <SimilarTable target={this.state.similarity_info.target}
                                  similar={this.state.similarity_info.similar}
                                  backHandler={this.clearSimilarityInfo.bind(this)}/>:
                    <Sentences sentences={this.state.sentences}
                               processResponse={this.processSimilarInfo.bind(this)}
                               clearHandler={this.clearSentences.bind(this)}/>
                }
            </div>
        </div>
  }
}

export default App;
