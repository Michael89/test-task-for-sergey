import React, { Component } from 'react';

class Header extends Component {

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <h2 className="navbar-brand">Sentence similarity app</h2>
            </nav>
        );
    }
}

export default Header;
