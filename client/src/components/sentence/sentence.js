import React, { Component } from 'react';

import './sentence.css'

class Sentence extends Component {

    render() {
        return (
            <span className="sentence" onClick={this.props.click}>
                {this.props.content}.&nbsp;
            </span>
        );
    }
}

export default Sentence;
