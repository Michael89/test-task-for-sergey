import React, { Component } from 'react';

class Button extends Component {

    render() {
        return <p className="lead">
                    <button type="button"
                            className="btn btn-outline-primary"
                            onClick={this.props.clickHandler}>
                        {this.props.value}
                    </button>
                </p>
  }
}

export default Button;
