import React, { Component } from 'react';

import Button from "../common/button";

class PasteComponent extends Component {

    predefined_sentences = [
        'A girl is styling her hair',
        'A girl is brushing her hair',
        'A group of men play soccer on the beach',
        'A group of boys are playing soccer on the beach',
        'One woman is measuring another woman\'s ankle',
        'A woman measures another woman\'s ankle',
        'A man is cutting up a cucumber',
        'A man is slicing a cucumber',
        'A man is playing a harp',
        'A man is playing a keyboard',
        'The young boys are playing outdoors and the man is smiling nearby',
        'A group of kids is playing in a yard and an old man is standing in the background',
        'Nobody is riding the bicycle on one wheel',
        'A person in a black jacket is doing tricks on a motorbike',
        'A man with a jersey is dunking the ball at a basketball game',
        'The player is missing the basket and a crowd is in background',
        'A group of friends are riding the current in a raft',
        'A group is not riding the current in a raft',
        'A family is watching a little boy who is hitting a baseball',
        'A family is watching a boy who is hitting a baseball',
        'A large group of Asian people is eating at a restaurant',
        'Various customers are eating in a crowded restaurant with purple lights'
    ];

    render() {
        return <div>
            <h2>Press Ctrl/Cmd+V to paste your data </h2>
            <Button value="Or click here to use predefined data"
                    clickHandler={()=>this.props.sentenceHandler(this.predefined_sentences)}/>
        </div>
  }
}

export default PasteComponent;
