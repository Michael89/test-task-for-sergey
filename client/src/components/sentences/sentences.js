import React, { Component } from 'react';

import Sentence from '../sentence/sentence'
import Button from "../common/button";

class Sentences extends Component {

    sentenceClick(current, all_sentences, index){
        return () => {
            let all_other = all_sentences.filter((v, idx) => idx!==index);
            fetch('/similarTable',
                {'method': 'POST',
                 'headers': {"Content-Type": "application/json"},
                 'body': JSON.stringify({'current': current, 'all_other': all_other})})
                .then((response)=>response.json())
                .then(this.props.processResponse)
        }
    }

    render() {
        return <div>

            {this.props.sentences.map((snts, index) =>
                <Sentence content={snts} key={index} click={this.sentenceClick(snts, this.props.sentences, index)}/>)}
            {this.props.sentences.length === 0 || <Button value="Clear" clickHandler={this.props.clearHandler}/>}

            </div>
  }
}

export default Sentences;
