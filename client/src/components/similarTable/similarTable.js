import React, { Component } from 'react';

import './table.css'
import Button from "../common/button";

class SimilarTable extends Component {

    render() {
        return <div>
                <h4>Results</h4>
                <table>
                    <thead>
                        <tr>
                            <td>{this.props.target}</td>
                            <td>Score</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.similar.map((similar_info, index)=>
                            <tr key={index} style={{backgroundColor: "rgba(0, 256, 0, " + similar_info["score"] + ")"}}>
                                <td>{similar_info["sentence"]}</td>
                                <td>{similar_info["score"]}</td>
                            </tr>)}
                    </tbody>
                </table>
                <Button value="Back" clickHandler={this.props.backHandler}/>
            </div>
  }
}

export default SimilarTable;
