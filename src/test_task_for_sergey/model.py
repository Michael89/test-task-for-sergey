import math
import os.path

import gensim
import numpy as np

from collections import Counter

from sklearn.metrics.pairwise import cosine_similarity

from test_task_for_sergey.domain import Sentence
from test_task_for_sergey.utils import read_tsv

PATH_TO_WORD2VEC = os.path.expanduser("~/Downloads/GoogleNews-vectors-negative300.bin")
PATH_TO_DOC_FREQUENCIES_FILE = os.path.expanduser("~/Downloads/doc_frequencies.tsv")


class SimilarityModel(object):

    def __init__(self):
        self.word2vec = gensim.models.KeyedVectors.load_word2vec_format(PATH_TO_WORD2VEC, binary=True)
        self._doc_freqs = read_tsv(PATH_TO_DOC_FREQUENCIES_FILE)
        self._docs_num = 1288431

    def _safe_get_embeddings(self, token):
        try:
            return token, self.word2vec[token]
        except KeyError:
            return None

    def compare_two_sentences(self, sentence1, sentence2):
        """
        :param sentence1: Sentence to compare with
        :type sentence1: Sentence

        :param sentence2: Sentence to compare with
        :type sentence2: Sentence

        :return: Cosine distance between average weighted vectors
        :rtype: float
        """

        tokens1 = sentence1.tokens_without_stop
        tokens2 = sentence2.tokens_without_stop

        tokfreqs1 = Counter(tokens1)
        tokfreqs2 = Counter(tokens2)

        token_to_embeddings1 = dict(filter(None, [self._safe_get_embeddings(token) for token in tokfreqs1]))
        token_to_embeddings2 = dict(filter(None, [self._safe_get_embeddings(token) for token in tokfreqs2]))

        if len(token_to_embeddings1) == 0 or len(token_to_embeddings2) == 0:
            return 0

        weights1 = [tokfreqs1[token] * math.log(self._docs_num / (self._doc_freqs.get(token, 0) + 1))
                    for token in token_to_embeddings1.keys()]
        weights2 = [tokfreqs2[token] * math.log(self._docs_num / (self._doc_freqs.get(token, 0) + 1))
                    for token in token_to_embeddings2.keys()]

        embedding1 = np.average(list(token_to_embeddings1.values()), axis=0, weights=weights1).reshape(1, -1)
        embedding2 = np.average(list(token_to_embeddings2.values()), axis=0, weights=weights2).reshape(1, -1)

        sim = cosine_similarity(embedding1, embedding2)[0][0]
        return sim

    def compare_raw_sentences(self, primary, others):
        """
        :param primary: Primary sentence
        :type primary: str

        :param others: Other sentence to compare with primary
        :type others: list[str]

        :return: List of other sentences ordered by score
        :rtype: list[dict]
        """
        result = []
        primary_sentence = Sentence(primary)
        for other in others:
            score = self.compare_two_sentences(primary_sentence, Sentence(other))
            result.append({'sentence': other, 'score': score})

        return sorted(result, key=lambda i: i['score'], reverse=True)
