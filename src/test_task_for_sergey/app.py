from pathlib import Path

from flask import Flask, request, jsonify
from test_task_for_sergey.core import SIMILARITY_MODEL

PATH_TO_STATIC = Path(__file__).absolute().parent.parent.parent.joinpath('client/build/').as_posix()

app = Flask(__name__, static_folder=PATH_TO_STATIC, static_url_path='')


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/similarTable', methods=['POST'])
def similarity():
    return jsonify({"target": request.json['current'],
                    "similar": SIMILARITY_MODEL.compare_raw_sentences(request.json['current'],
                                                                      request.json['all_other'])})
