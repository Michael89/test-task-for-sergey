#!/bin/bash

set -e              # to make script exit as soon as any command fails

pushd `dirname $0` > /dev/null

echo "Build server"
virtualenv -p python3.6 ./runtime
source ./runtime/bin/activate
pip install -U -r requirenments.txt
deactivate

bin/python -c "import nltk; nltk.download(['stopwords', 'punkt'])"

echo "Build client"

cd client
npm run build

popd > /dev/null